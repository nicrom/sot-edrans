# sot-edrans

This repository is part of the Docker course for Edrans SoT

## Roadmap Challenge

 Deployment wordpress site in a target VM using docker, docker-compose.

 Requeriments:

 1. Wordpress needs to be accessible over the internet using a public URL. i.e.: sot-wordpress.duckdns.org
 2. Site data must be persistent
 3. The site should support HTTPS access
 4. Wordpress needs to coexist in the target VM along with our other project: https://gitlab.com/nicrom/sot-edrans-app/


![Diagram](diagram.png "Docker diagram")

Website 1: Wordpress

Website 2: sot-edrans-app
